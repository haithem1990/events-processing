package com.events.processing.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Process1Step2 {

    @Id
    @GeneratedValue
    private Long id;

    private String stepId;

    private String processId;

    public Process1Step2() {
    }

    public Process1Step2(String stepId, String processId) {
        this.stepId = stepId;
        this.processId = processId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }
}
