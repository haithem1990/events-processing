package com.events.processing.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ProcessTraceability {

    @Id
    private String processId;

    private String processStatus;

    private String descException;

    public ProcessTraceability() {
    }

    public ProcessTraceability(String processId, String processStatus) {
        this.processId = processId;
        this.processStatus = processStatus;
    }

    public ProcessTraceability(String processId, String processStatus, String descException) {
        this.processId = processId;
        this.processStatus = processStatus;
        this.descException = descException;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public String getDescException() {
        return descException;
    }

    public void setDescException(String descException) {
        this.descException = descException;
    }
}
