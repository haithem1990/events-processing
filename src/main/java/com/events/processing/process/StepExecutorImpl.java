package com.events.processing.process;

import com.events.processing.model.ProcessContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class StepExecutorImpl implements StepExecutor {

    @Override
    @Transactional
    public void execute(List<Step> stepList, ProcessContext processContext) {
          for(Step step : stepList){
            // execute step
            processContext = step.start(processContext);
        }
    }
}
