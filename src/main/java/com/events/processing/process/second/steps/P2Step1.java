package com.events.processing.process.second.steps;

import com.events.processing.exceptions.StepExecutionException;
import com.events.processing.model.ProcessContext;
import com.events.processing.process.Step;
import org.springframework.stereotype.Component;

@Component
public class P2Step1 implements Step {

    @Override
    public ProcessContext start(ProcessContext processContext) {
        try {
            System.out.println("**************************************");
        return processContext;
        } catch(Exception e){
            throw new StepExecutionException("P2S1 -----> failure",e);
        }
    }
}
