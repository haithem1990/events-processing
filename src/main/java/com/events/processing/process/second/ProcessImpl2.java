package com.events.processing.process.second;

import com.events.processing.model.OnMessageData;
import com.events.processing.model.ProcessContext;
import com.events.processing.process.ProcessTemplate;
import com.events.processing.process.Processor;
import com.events.processing.process.Step;
import com.events.processing.process.StepExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProcessImpl2 implements Processor {

    private final ProcessTemplate processTemplate;
    private final StepExecutor stepExecutor;

    @Autowired
    public ProcessImpl2(@Qualifier("processTemplate2") ProcessTemplate processTemplate, StepExecutor stepExecutor) {
       this.processTemplate = processTemplate;
       this.stepExecutor=stepExecutor;
    }

    @Override
    public void process(OnMessageData onMessageData) {
        // build the template
        List<Step> stepList = processTemplate.build();
        // initialize the context
        ProcessContext processContext = new ProcessContext(onMessageData,this.getId(),this.getPreviousId());
        // execute steps
        stepExecutor.execute(stepList,processContext);
    }

    @Override
    public String getId() {
        return "2";
    }

    public String getPreviousId() {
        return "1";
    }
}
