package com.events.processing.process.second.template;

import com.events.processing.process.ProcessTemplate;
import com.events.processing.process.Step;
import com.events.processing.process.second.steps.P2Step1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class ProcessTemplate2 implements ProcessTemplate {

    private final P2Step1 p2Step1;


    @Autowired
    public ProcessTemplate2(P2Step1 p2Step1) {
        this.p2Step1 = p2Step1;
    }

    public List<Step> build(){
        return  Arrays.asList(
                p2Step1
        );
    }
}
