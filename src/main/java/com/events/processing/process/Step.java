package com.events.processing.process;

import com.events.processing.model.ProcessContext;

public interface Step {

    ProcessContext start(ProcessContext processContext);
}
