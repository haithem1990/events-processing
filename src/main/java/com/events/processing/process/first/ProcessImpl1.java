package com.events.processing.process.first;

import com.events.processing.model.OnMessageData;
import com.events.processing.model.ProcessContext;
import com.events.processing.process.ProcessTemplate;
import com.events.processing.process.Processor;
import com.events.processing.process.Step;
import com.events.processing.process.StepExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProcessImpl1 implements Processor {

    private final StepExecutor stepExecutor;
    private final ProcessTemplate processTemplate;

    @Autowired
    public ProcessImpl1(StepExecutor stepExecutor,@Qualifier("processTemplate1") ProcessTemplate processTemplate) {
        this.stepExecutor=stepExecutor;
        this.processTemplate = processTemplate;
    }

    @Override
    public void process(OnMessageData onMessageData) {
        // build the template
        List<Step> stepList = processTemplate.build();
        // initialize the context
        ProcessContext processContext = new ProcessContext(onMessageData,this.getId(),this.getPreviousId());
        // execute steps
        stepExecutor.execute(stepList,processContext);
    }

    @Override
    public String getId() {
        return "1";
    }

    public String getPreviousId() {
        return "0";
    }
}
