package com.events.processing.process.first.steps;

import com.events.processing.entities.Process1Step5;
import com.events.processing.exceptions.StepExecutionException;
import com.events.processing.model.ProcessContext;
import com.events.processing.process.Step;
import com.events.processing.repositories.Process1Step5Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class P1S5 implements Step {
    private final Process1Step5Repository process1Step5Repository;

    @Autowired
    public P1S5(Process1Step5Repository process1Step5Repository) {
        this.process1Step5Repository = process1Step5Repository;
    }

    @Override
    public ProcessContext start(ProcessContext processContext) {
        try{
            Process1Step5 process1Step5 = process1Step5Repository.save(new Process1Step5("5",processContext.getCurrentProcessId()));
        processContext.put("process1Step5",process1Step5);
        System.out.println("P1S5 ------> success");
        return processContext;
    } catch(Exception e){
        throw new StepExecutionException("P1S5 -----> failure",e);
    }
    }
}
