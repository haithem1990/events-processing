package com.events.processing.process.first.steps;


import com.events.processing.exceptions.StepExecutionException;
import com.events.processing.model.ProcessContext;
import com.events.processing.process.Step;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;

@Component
public class P1S4 implements Step {
    @Override
    public ProcessContext start(ProcessContext processContext) {
        try{
            RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        restTemplateBuilder.build().getForObject("localhost:8095/mathemech",String.class);
        System.out.println("P1S4 ------> success");
        return processContext;
    } catch(Exception e){
        throw new StepExecutionException("P1S4 -----> failure",e);
    }
    }

}
