package com.events.processing.process.first.steps;

import com.events.processing.entities.Process1Step1;
import com.events.processing.exceptions.StepExecutionException;
import com.events.processing.model.ProcessContext;
import com.events.processing.process.Step;
import com.events.processing.repositories.Process1Step1Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class P1S1 implements Step {

    private final Process1Step1Repository process1Step1Repository;

    @Autowired
    public P1S1(Process1Step1Repository process1Step1Repository) {
        this.process1Step1Repository = process1Step1Repository;
    }

    @Override
    public ProcessContext start(ProcessContext processContext) {
        try{
        Process1Step1 process1Step1 = process1Step1Repository.save(new Process1Step1("1",processContext.getCurrentProcessId()));
        processContext.put("process1Step1",process1Step1);
        System.out.println("P1S1 ------> success");
        return processContext;
        } catch(Exception e){
            throw new StepExecutionException("P1S1 -----> failure",e);
        }
    }
}
