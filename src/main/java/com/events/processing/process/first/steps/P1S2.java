package com.events.processing.process.first.steps;

import com.events.processing.entities.Process1Step2;
import com.events.processing.exceptions.StepExecutionException;
import com.events.processing.model.ProcessContext;
import com.events.processing.process.Step;
import com.events.processing.repositories.Process1Step2Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class P1S2 implements Step {
    private final Process1Step2Repository process1Step2Repository;

    @Autowired
    public P1S2(Process1Step2Repository process1Step2Repository) {
        this.process1Step2Repository = process1Step2Repository;
    }

    @Override
    public ProcessContext start(ProcessContext processContext) {
        try{
            Process1Step2 process1Step2 = process1Step2Repository.save(new Process1Step2("2",processContext.getCurrentProcessId()));
        processContext.put("process1Step2",process1Step2);
        System.out.println("P1S2 ------> success");
        return processContext;
    } catch(Exception e){
        throw new StepExecutionException("P1S2 -----> failure",e);
    }
    }
}
