package com.events.processing.process.first.template;

import com.events.processing.process.ProcessTemplate;
import com.events.processing.process.Step;
import com.events.processing.process.first.steps.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
@Component
public class ProcessTemplate1 implements ProcessTemplate {

    private final P1S1 p1S1;
    private final P1S2 p1S2;
    private final P1S3 p1S3;
    private final P1S4 p1S4;
    private final P1S5 p1S5;

    @Autowired
    public ProcessTemplate1(P1S1 p1S1, P1S2 p1S2, P1S3 p1S3, P1S4 p1S4, P1S5 p1S5) {
        this.p1S1 = p1S1;
        this.p1S2 = p1S2;
        this.p1S3 = p1S3;
        this.p1S4 = p1S4;
        this.p1S5 = p1S5;
    }

    public List<Step> build(){
        return  Arrays.asList(
                p1S1,
                p1S2,
                p1S3,
                p1S4,
                p1S5
        );
    }
}
