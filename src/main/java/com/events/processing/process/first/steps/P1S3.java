package com.events.processing.process.first.steps;

import com.events.processing.entities.Process1Step3;
import com.events.processing.exceptions.StepExecutionException;
import com.events.processing.model.ProcessContext;
import com.events.processing.process.Step;
import com.events.processing.repositories.Process1Step3Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class P1S3 implements Step {
    private final Process1Step3Repository process1Step3Repository;

    @Autowired
    public P1S3(Process1Step3Repository process1Step3Repository) {
        this.process1Step3Repository = process1Step3Repository;
    }

    @Override
    public ProcessContext start(ProcessContext processContext) {
        try{
            Process1Step3 process1Step3 = process1Step3Repository.save(new Process1Step3("3",processContext.getCurrentProcessId()));
        processContext.put("process1Step3",process1Step3);
        System.out.println("P1S3 ------> success");
        return processContext;
    } catch(Exception e){
        throw new StepExecutionException("P1S3 -----> failure",e);
    }
    }
}
