package com.events.processing.process;

import com.events.processing.model.OnMessageData;

public interface Processor {

    void process(OnMessageData onMessageData);

    String getId();

    String getPreviousId();
}
