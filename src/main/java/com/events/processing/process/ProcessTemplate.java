package com.events.processing.process;

import java.util.List;

public interface ProcessTemplate {

    List<Step> build();

}
