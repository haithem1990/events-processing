package com.events.processing.process;

import com.events.processing.model.ProcessContext;

import java.util.List;

public interface StepExecutor {

    void execute(List<Step> stepList, ProcessContext processContext);
}
