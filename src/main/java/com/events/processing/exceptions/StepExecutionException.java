package com.events.processing.exceptions;

public class StepExecutionException extends ApplicationException {

    public StepExecutionException(Throwable throwable) {
        super(throwable);
    }

    public StepExecutionException(String message,Throwable throwable) {
        super(message, throwable);
    }
}
