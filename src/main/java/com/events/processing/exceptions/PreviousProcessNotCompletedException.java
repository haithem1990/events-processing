package com.events.processing.exceptions;

public class PreviousProcessNotCompletedException extends ApplicationException {

    public PreviousProcessNotCompletedException(Throwable throwable) {
        super(throwable);
    }

    public PreviousProcessNotCompletedException(String message) {
        super(message);
    }
}
