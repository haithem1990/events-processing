package com.events.processing.exceptions;

public abstract class ApplicationException extends RuntimeException {

    public ApplicationException(Throwable throwable){
        super(throwable);
    }

    public ApplicationException(String message,Throwable throwable){
        super(message, throwable);
    }

    public ApplicationException(String message){
        super(message);
    }
}
