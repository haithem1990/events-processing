package com.events.processing.repositories;

import com.events.processing.entities.Process1Step1;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Process1Step1Repository extends JpaRepository<Process1Step1,Long> {
}
