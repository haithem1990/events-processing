package com.events.processing.repositories;

import com.events.processing.entities.ProcessTraceability;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProcessTraceabilityRepository extends JpaRepository<ProcessTraceability,String> {

    Optional<ProcessTraceability> findByProcessId(String processId);
}
