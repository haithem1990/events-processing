package com.events.processing.repositories;

import com.events.processing.entities.Process1Step2;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Process1Step2Repository extends JpaRepository<Process1Step2,Long> {
}
