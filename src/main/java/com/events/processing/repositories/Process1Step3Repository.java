package com.events.processing.repositories;

import com.events.processing.entities.Process1Step3;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Process1Step3Repository extends JpaRepository<Process1Step3,Long> {
}
