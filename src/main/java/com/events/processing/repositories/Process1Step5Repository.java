package com.events.processing.repositories;

import com.events.processing.entities.Process1Step5;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Process1Step5Repository extends JpaRepository<Process1Step5,Long> {
}
