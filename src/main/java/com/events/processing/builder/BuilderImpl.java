package com.events.processing.builder;

import com.events.processing.process.Processor;
import com.events.processing.provider.ProcessorProvider;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class BuilderImpl{
    @EventListener
    public void onStartUp(ApplicationStartedEvent applicationStartedEvent) {
        Map<String, Processor> processorMap = applicationStartedEvent.getApplicationContext().getBeansOfType(Processor.class);
        ProcessorProvider processorProvider = applicationStartedEvent.getApplicationContext().getBean("processorProviderImpl",ProcessorProvider.class);
        processorMap.forEach((key,value)->processorProvider.put(value));
    }

}
