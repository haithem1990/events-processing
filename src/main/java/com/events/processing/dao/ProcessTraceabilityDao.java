package com.events.processing.dao;

import com.events.processing.entities.ProcessTraceability;

public interface ProcessTraceabilityDao {

    ProcessTraceability findByProcessId(String processId);
    ProcessTraceability save(ProcessTraceability processTraceability);
    ProcessTraceability update(ProcessTraceability processTraceability);
}
