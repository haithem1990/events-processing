package com.events.processing.dao;

import com.events.processing.entities.ProcessTraceability;
import com.events.processing.repositories.ProcessTraceabilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProcessTraceabilityDaoImpl implements ProcessTraceabilityDao{

    private final ProcessTraceabilityRepository processTraceabilityRepository;

    @Autowired
    public ProcessTraceabilityDaoImpl(ProcessTraceabilityRepository processTraceabilityRepository) {
        this.processTraceabilityRepository = processTraceabilityRepository;
    }

    @Override
    public ProcessTraceability findByProcessId(String processId) {
        // ToDo quick and dirty
        // ToDo to be optimized with exception handliung
        return processTraceabilityRepository.findByProcessId(processId).get();
    }

    @Override
    public ProcessTraceability save(ProcessTraceability processTraceability) {
        // ToDo quick and dirty
        // ToDo to be optimized with exception handling
        return processTraceabilityRepository.save(processTraceability);
    }

    @Override
    public ProcessTraceability update(ProcessTraceability processTraceability) {
        // ToDo quick and dirty
        // ToDo to be optimized with exception handling
        return processTraceabilityRepository.save(processTraceability);
    }
}
