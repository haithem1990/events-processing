package com.events.processing;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication
@EnableTransactionManagement
public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(Main.class);

        WorkFlow workFlow = applicationContext.getBean("workFlow",WorkFlow.class);
        KafkaTemplate<Object,Object> kafkaTemplate = applicationContext.getBean("kafkaTemplate",KafkaTemplate.class);

        kafkaTemplate.send(new ProducerRecord<>("operation","1"));
        kafkaTemplate.send(new ProducerRecord<>("operation","2"));
    }
}
