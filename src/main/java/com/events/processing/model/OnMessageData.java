package com.events.processing.model;

public class OnMessageData {
    private String id;

    public OnMessageData(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
