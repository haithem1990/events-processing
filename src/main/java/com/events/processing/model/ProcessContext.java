package com.events.processing.model;

import java.util.HashMap;
import java.util.Map;

public class ProcessContext {

    private Map<String,Object> contextMap = new HashMap<>();

    private OnMessageData onMessageData;

    private String currentProcessId;

    private String previousProcessId;

    public ProcessContext(OnMessageData onMessageData,String currentProcessId,String previousProcessId) {
        this.onMessageData = onMessageData;
        this.currentProcessId=currentProcessId;
        this.previousProcessId=previousProcessId;
    }

    public void put(String key,Object value) {
        this.getContextMap().put(key,value);
    }

        public Map<String, Object> getContextMap() {
        return contextMap;
    }

    public void setContextMap(Map<String, Object> contextMap) {
        this.contextMap = contextMap;
    }

    public OnMessageData getOnMessageData() {
        return onMessageData;
    }

    public void setOnMessageData(OnMessageData onMessageData) {
        this.onMessageData = onMessageData;
    }

    public String getCurrentProcessId() {
        return currentProcessId;
    }

    public void setCurrentProcessId(String currentProcessId) {
        this.currentProcessId = currentProcessId;
    }

    public String getPreviousProcessId() {
        return previousProcessId;
    }

    public void setPreviousProcessId(String previousProcessId) {
        this.previousProcessId = previousProcessId;
    }
}
