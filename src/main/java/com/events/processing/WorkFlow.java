package com.events.processing;

import com.events.processing.entities.ProcessTraceability;
import com.events.processing.model.OnMessageData;
import com.events.processing.process.Processor;
import com.events.processing.provider.ProcessorProvider;
import com.events.processing.services.ProcessTraceabilityService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class WorkFlow {

    private ProcessorProvider processorProvider;
    private final ProcessTraceabilityService processTraceabilityService;

    public WorkFlow(ProcessorProvider processorProvider,ProcessTraceabilityService processTraceabilityService) {
        this.processorProvider = processorProvider;
        this.processTraceabilityService=processTraceabilityService;
    }

    @KafkaListener(topics = "operation")
    public void onMessage(String id){

        System.out.println("================== > id = " + id);
        // retrieve the target processor from the processor provider by id
        Processor processor = processorProvider.findProcessorById(id);
        try {
            // check if the previous process finish with success
            if(!processor.getPreviousId().equals("0"))
                processTraceabilityService.checkProcessValidation(processor.getPreviousId());
            processor.process(new OnMessageData(id));
            // save process was completed successfully
            processTraceabilityService.save(new ProcessTraceability(processor.getId(),"completed"));
        } catch (Exception e){
            System.out.println(e.getMessage());
            processTraceabilityService.save(new ProcessTraceability(processor.getId() ,"not_completed",e.getMessage()));
        }
    }
}