package com.events.processing.provider;

import com.events.processing.process.Processor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
@Component
public class ProcessorProviderImpl implements ProcessorProvider {

    private Map<String, Processor> processorMap;

    @Override
    public Processor findProcessorById(String id) {
        return processorMap.get(id);
    }

    public void put(Processor processor){
        if(this.processorMap==null)
            this.processorMap=new HashMap<>();
        this.processorMap.put(processor.getId(),processor);
    }

}
