package com.events.processing.provider;

import com.events.processing.process.Processor;

public interface ProcessorProvider {

    Processor findProcessorById(String id);

    void put(Processor processor);
}
