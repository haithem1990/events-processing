package com.events.processing.services;

import com.events.processing.dao.ProcessTraceabilityDao;
import com.events.processing.entities.ProcessTraceability;
import com.events.processing.exceptions.PreviousProcessNotCompletedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcessTraceabilityServiceImpl implements ProcessTraceabilityService {
    
    private final ProcessTraceabilityDao processTraceabilityDao;
    
    @Autowired
    public ProcessTraceabilityServiceImpl(ProcessTraceabilityDao processTraceabilityDao) {
        this.processTraceabilityDao = processTraceabilityDao;
    }

    @Override
    public ProcessTraceability findByProcessId(String processId) {
        return processTraceabilityDao.findByProcessId(processId);
    }

    @Override
    public ProcessTraceability save(ProcessTraceability processTraceability) {
        // ToDo quick and dirty
        // ToDo to be optimized with exception handling
        return processTraceabilityDao.save(processTraceability);
    }

    @Override
    public ProcessTraceability update(ProcessTraceability processTraceability) {
        // ToDo quick and dirty
        // ToDo to be optimized with exception handling
        return processTraceabilityDao.save(processTraceability);
    }

    @Override
    public void checkProcessValidation(String id) {
        ProcessTraceability processTraceability = this.findByProcessId(id);
        if(processTraceability.getProcessStatus().equals("not_completed")){
            throw new PreviousProcessNotCompletedException("process "+ id + " not completed");
        }
    }
}
