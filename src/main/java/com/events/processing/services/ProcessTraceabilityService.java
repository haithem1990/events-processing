package com.events.processing.services;

import com.events.processing.entities.ProcessTraceability;

public interface ProcessTraceabilityService {

    ProcessTraceability findByProcessId(String processId);

    ProcessTraceability save(ProcessTraceability processTraceability);

    ProcessTraceability update(ProcessTraceability processTraceability);

    void checkProcessValidation(String id);
}
